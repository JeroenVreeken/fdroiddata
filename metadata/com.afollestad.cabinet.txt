AntiFeatures:NonFreeDep
Categories:Office
License:MIT
Web Site:https://github.com/afollestad/cabinet/blob/HEAD/README.md
Source Code:https://github.com/afollestad/cabinet
Issue Tracker:https://github.com/afollestad/cabinet/issues

Auto Name:Cabinet Beta
Summary:File manager
Description:
Minimal file manager designed for Android 4.1 and above. However, updates
might be slow or might not come at all, since the author has no longer the
time to maintain it. Pullrequests are welcome.
.

Repo Type:git
Repo:https://github.com/afollestad/cabinet

Build:1.8.1,71
    commit=a6440d5bd4dd95a8416a86305527a6f86aa49020
    subdir=app
    gradle=yes
    rm=app/libs/*
    prebuild=sed -i -e '/fileTree/acompile "com.jcraft:jsch:0.1.51"\ncompile "com.anjlab.android.iab.v3:library:1.0.8@aar"' build.gradle

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.8.1
Current Version Code:71

